import React from "react";
import { Routes, Route } from "react-router-dom";
import logo from "../assets/star-wars-logo.svg";
import "../css/index.css";

import { RequireAuth } from "./features/auth/requireAuth";

import { Login } from "./features/login/login";
import { Register } from "./features/register/register";
import { ForgotPassword } from "./features/password/forgotPassword";
import { ResetPassword } from "./features/password/resetPassword";
import { Home } from "./features/users/home";
import { Characters } from "./features/characters/characters";
import { Planets } from "./features/planets/planets";
import { Ships } from "./features/ships/ships";


function App() {

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <Routes>
        <Route>
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
          <Route path="/reset-password" element={<ResetPassword />} />
          <Route
            path="/"
            element={
              <RequireAuth>
                <Home />
              </RequireAuth>
            }
          />
          <Route
            path="/characters"
            element={
              <RequireAuth>
                <Characters />
              </RequireAuth>
            }
          />
          <Route
            path="/planets"
            element={
              <RequireAuth>
                <Planets />
              </RequireAuth>
            }
          />
          <Route
            path="/starships"
            element={
              <RequireAuth>
                <Ships />
              </RequireAuth>
            }
          />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
