import { configureStore } from '@reduxjs/toolkit'

//Services
import { authApi } from './services/authApi'
import {peopleApi} from './services/charactersApi'
import {planetsApi} from './services/planetsApi'
import {shipsApi} from './services/shipsApi'

//Features
import counterReducer from '../features/counter/counterSlice'
import auth from '../features/auth/authSlice'


export const store = configureStore({
    reducer: {
        [authApi.reducerPath]: authApi.reducer,
        [peopleApi.reducerPath]: peopleApi.reducer,
        [planetsApi.reducerPath]: planetsApi.reducer,
        [shipsApi.reducerPath]: shipsApi.reducer,
        counter: counterReducer,
        auth,
    },
})