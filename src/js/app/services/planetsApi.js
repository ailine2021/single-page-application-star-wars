import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const planetsApi = createApi({
  reducerPath: "planetsApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://swapi.dev/api/",
  }),
  endpoints(builder) {
    return {
      fetchPlanets: builder.query({
        query(limit = 10) {
          return `/planets?limit=${limit}`;
        },
      }),
    };
  },
});

export const { useFetchPlanetsQuery } = planetsApi;
