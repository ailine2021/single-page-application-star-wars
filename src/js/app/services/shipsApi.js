import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const shipsApi = createApi({
    reducerPath: 'shipsApi',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://swapi.dev/api/'
    }),
    endpoints(builder){
        return{
            fetchShips: builder.query({
                query(limit =10){
                    return `/starships?limit=${limit}`;  
                }
            })
        }
    }
})

export const {useFetchShipsQuery} = shipsApi
