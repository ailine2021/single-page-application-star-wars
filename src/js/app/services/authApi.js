import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const authApi = createApi({
  reducerPath: "authApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://api.pote.dev",
  }),

  endpoints(builder) {
    return {
      login: builder.mutation({
        query: (body) => ({
          url: "/auth/login",
          method: "POST",
          body,
        }),
      }),
      register: builder.mutation({
        query: (body) => ({
          url: "/users",
          method: "POST",
          body,
        }),
      }),
      forgotPassword: builder.mutation({
        query: (body) => ({
          url: "/auth/forgot_password",
          method: "POST",
          body,
        }),
      }),
      resetPassword: builder.mutation({
        query: (body) => ({
          url: "/auth/reset_password",
          method: "POST",
          body,
        }),
      }),
    };
  },
});

export const {
  useLoginMutation,
  useRegisterMutation,
  useForgotPasswordMutation,
  useResetPasswordMutation,
} = authApi;
