import React, { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import { useForgotPasswordMutation } from "../../app/services/authApi";

import { InputGroup } from "../form/InputGroup";
import { SubmitButton } from "../form/SubmitButton";

export const ForgotPassword = () => {
  const navigate = useNavigate();

  //body
  const [email, setEmail] = useState("");

  //Logic
  const [forgotPassword, { isLoading, isUpdating }] =
    useForgotPasswordMutation();

  const [formError, setFormError] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();

    setFormError(null);

    const body = {
      email,
    };

    try {
      const result = await forgotPassword(body);

      if (result.error) {
        return setFormError(result.error.data.message);
      }

      navigate("/login");
    } catch (err) {
      console.log("Something went wrong", err);
    }
  };

  return (
    <div>
      <h1>Forgot Password</h1>
      <p>Indiquer l'adresse mail utilisée au moment de l'inscription</p>
      <p style={{ color: "red" }}>{formError && formError}</p>
      {isLoading && <p>Loading...</p>}
      <form onSubmit={handleSubmit}>
        <InputGroup
          handleChange={setEmail}
          label="Email"
          type="email"
          required
        />
        <SubmitButton name="Send" />
      </form>
      <p>
        Return to <Link to="/login">Sign In</Link>
      </p>
    </div>
  );
};
