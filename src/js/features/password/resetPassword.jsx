import React, { useState } from "react";
import { useNavigate, Link } from "react-router-dom";

import { useResetPasswordMutation } from "../../app/services/authApi";

import { InputGroup } from "../form/InputGroup";
import { SubmitButton } from "../form/SubmitButton";

export const ResetPassword = () => {

    const navigate = useNavigate();

    // Body
    const [password, setPassword] = useState("");
    const [token, setToken] = useState("");

    // Logic

    const [formError, setFormError] = useState(null);
    const [resetPassword, { isLoading, isUpdating }] = useResetPasswordMutation();

    const handleSubmit = async (e) => {
        e.preventDefault();
    
        setFormError(null);
    
        const body = {
          password,
        };
    
        try {
          const result = await resetPassword(body);
    
          if (result.error) {
            return setFormError(result.error.data.message);
          }
    
          navigate("/login");
        } catch (err) {
          console.log("Something went wrong", err);
        }
      };


  return (
    <div>
      <h1>Reset password</h1>
      <p style={{ color: "red" }}>{formError && formError}</p>
      {isLoading && <p>Loading...</p>}
      <form onSubmit={handleSubmit}>
        <InputGroup
          handleChange={setToken}
          label="Token"
          type="text"
        />
        <InputGroup
          handleChange={setPassword}
          label="Password"
          type="password"
          required={true}
          minLength="1"
          maxLength="15"
        />
        <InputGroup
          handleChange={setPassword}
          label="Confirm Password"
          type="password"
          required={true}
          minLength="1"
          maxLength="15"
        />
        <SubmitButton name="Change Password" />
      </form>
      <p>
        Return to <Link to="/login">Sign In</Link>
      </p>
    </div>
  );
};
