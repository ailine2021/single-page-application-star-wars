import React, { useState } from "react";
import { useRegisterMutation } from "../../app/services/authApi";
import { useNavigate, Link } from "react-router-dom";

import { InputGroup } from "../form/InputGroup";
import { SubmitButton } from "../form/SubmitButton";

export const Register = () => {
  const navigate = useNavigate();

  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");

  const [register, { isLoading, isUpdating }] = useRegisterMutation();

  const [formError, setFormError] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();

    setFormError(null);

    const body = {
      email,
      password,
      username,
      firstname,
      lastname,
    };

    try {
      const result = await register(body);

      if (result.error) {
        return setFormError(result.error.data.message);
      }

      navigate("/login");
    } catch (err) {
      console.log("Something went wrong", err);
    }
  };

  return (
    <div>
      <h1>Register</h1>
      <p style={{ color: "red" }}>{formError && formError}</p>
      {isLoading && <p>Loading...</p>}
      <form onSubmit={handleSubmit}>
        <InputGroup
          handleChange={setEmail}
          label="Email"
          type="email"
          required
        />
        <InputGroup
          handleChange={setPassword}
          label="Password"
          type="password"
          required={true}
          minLength="1"
          maxLength="15"
        />
        <InputGroup
          handleChange={setLastname}
          label="Lastname"
          type="text"
          required={true}
        />
        <InputGroup
          handleChange={setFirstname}
          label="firstname"
          type="text"
          required={true}
        />
        <InputGroup
          handleChange={setUsername}
          label="username"
          type="text"
          required={true}
        />
        <SubmitButton name="Sign Up" />
      </form>
      <p>Already have an account? Please<Link to="/login">Sign In</Link></p>
    </div>
  );
};
