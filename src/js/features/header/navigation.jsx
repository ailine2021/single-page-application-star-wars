import React from 'react'
import {Link} from 'react-router-dom'

export const NavBar= (props) =>{
    return (
        <nav style={{backgroundColor:'red',padding:'10px', width: '100vw'}}>
        <ul>
            <li>
                <Link to="/">Home</Link>
            </li>
            <li>
                <Link to="/characters">Characters</Link>
            </li>
            <li>
                <Link to="/planets">Planets</Link>
            </li>
            <li>
                <Link to="/starships">Starships</Link>
            </li>
        </ul>
    </nav>
    );
}

export default NavBar;