import React from "react";

import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { logout } from "../auth/authSlice";
import { removeLocalStorageItem } from '../../utils/localStorage';

import logo from "../../../assets/star-wars-logo.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { faUserSlash } from "@fortawesome/free-solid-svg-icons";

export const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.auth.user);

  const handleLogout = () => {
    removeLocalStorageItem('xsrfToken')
    removeLocalStorageItem('accessToken')
    dispatch(logout());
    navigate("/login");
  };

  return (
    <header className="App-header"style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
      <img style={{ width: "10%" }} src={logo} className="App-logo" alt="logo" />
      <p>{currentUser?.username}</p><Link to='/profile'><FontAwesomeIcon icon={faUser} /></Link>
      <FontAwesomeIcon style={{cursor: "pointer" }} onClick={handleLogout} icon={faUserSlash} />
    </header>
  );
};

