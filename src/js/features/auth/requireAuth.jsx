import React from 'react'
import { useSelector } from 'react-redux'
import { useLocation, Navigate } from 'react-router-dom'

export const RequireAuth = ({ children }) => {

    const auth = useSelector(state => state.auth)
    let location = useLocation();

    console.log('auth is Authenticated : ', auth.isAuthenticated);

    if (!auth.isAuthenticated) {
        return <Navigate to="/login" state={{ from: location }} />;
    }

    return children;
}