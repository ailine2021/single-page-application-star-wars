import React from "react";
import { useFetchPlanetsQuery } from "../../app/services/planetsApi";

export const Planets = () => {
  const { data, isFetching } = useFetchPlanetsQuery();

  if (isFetching) return <div>Planets loading ...</div>;

  return (
    <div>
      <h1>Planets</h1>

      {data?.results?.map((planet) => {
        return (
          <div key={planet.name}>
            <p>{planet.name}</p>
          </div>
        );
      })}
    </div>
  );
};
