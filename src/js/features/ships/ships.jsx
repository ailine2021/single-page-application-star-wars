import React from "react";
import { useFetchShipsQuery } from "../../app/services/shipsApi";

export const Ships = () => {
  const { data, isFetching } = useFetchShipsQuery();

  if (isFetching) return <div>Starships loading ...</div>;

  return (
    <div>
      <h1>Staships</h1>

      {data?.results?.map((ship) => {
        return (
          <div key={ship.name}>
            <p>{ship.name}</p>
          </div>
        );
      })}
    </div>
  );
};
