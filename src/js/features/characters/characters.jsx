import React from 'react'
import { useFetchPeopleQuery } from '../../app/services/charactersApi'

export const Characters = () => {
    const { data, isFetching } = useFetchPeopleQuery();

    if (isFetching) return <div>Characters loading ...</div>

    return <div>
        <h1>Characters</h1>
        {
            data?.results?.map(people => {
                return <div key={people.name}>
                    <p>{people.name}</p>
                </div>
            })
        }
    </div>
}